//
//  ViewController.m
//  Fusion Client iOS SDK App
//
//

#import "ViewController.h"
#import "ConfigViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////////////////////////////
//
// UI Methods

-(IBAction)dial:(id)sender
{
    [self logMessage: [NSString stringWithFormat:@"User dialed %@", self.number.text]];
}

-(IBAction)hangup:(id)sender
{
    [self logMessage:@"User hungup"];
}

-(IBAction)toggleMedia:(id)sender
{
    [self logMessage:[NSString stringWithFormat:
                      @"User video: %d, voice: %d",
                      self.video.on, self.audio.on] ];
}

-(IBAction)userDoneEnteringText:(id)sender
{
    [sender resignFirstResponder];
}

-(void)logMessage:(NSString*)message
{
    self.log.text = [NSString stringWithFormat:@"%@\n%@", message, self.log.text];
    NSLog(@":::%@", message);
}

@end